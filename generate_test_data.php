<?php
require_once __DIR__ . '/vendor/autoload.php';

use Faker\Generator;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

// Load the Swagger API Spec
$apiSpec = json_decode(file_get_contents('clio_swagger.json'), true);

// Get the paths supported by the API
$paths = Arr::get($apiSpec, 'paths', []);

// Get the schemas that are expected in the responses
$apiSchemas = Arr::get($apiSpec, 'components.schemas', []);

// Build a lookup dictionary of classes as keys, and the namespace they belong to as values.
$xTags = Arr::get($apiSpec, 'x-tagGroups', []);
$namespaces = [];

foreach ($xTags as $xTag) {
    $xTagTags = Arr::get($xTag, 'tags', []);

    foreach ($xTagTags as $xTagTag) {
        $xTagTag = Str::singular(preg_replace('/[^A-Za-z0-9]/', '', $xTagTag));
        $namespaces[$xTagTag] = preg_replace('/[^A-Za-z0-9]/', '', $xTag['name']);
    }
}

// These classes will be skipped (i.e. the class is already working and we don't want to overwrite)
// Formatted as: [Namespace => [Class Name, Class Name]]
$ignoredClasses = [];

foreach ($paths as $path => $methods) {
    // Normally the base path will be the directory name of the path
    $basePath = ltrim(pathinfo($path, PATHINFO_DIRNAME), '/');

    // If the base path directory name is empty, use the filename of the path (without the extension)
    if (empty($basePath)) {
        $basePath = ltrim(pathinfo($path, PATHINFO_FILENAME), '/');
    }

    // Get the first HTTP method spec from the path
    $firstMethod = Arr::first($methods);

    // Get the tags of the first HTTP method
    $firstMethodTags = Arr::get($firstMethod, 'tags');

    // The class name we expect to use will be the first tag provided.
    // Converted to StudlyCase and non-alphanumeric characters are removed.
    $className = preg_replace('/[^A-Za-z0-9]/', '', Str::singular(Str::studly(Arr::first($firstMethodTags))));

    // Lookup the namespace. If it's not found, use the class name.
    $namespace = Arr::has($namespaces, $className) ? $namespaces[$className] : Str::studly(Arr::first($firstMethodTags));

    if ($className === 'BillingSetting') {
        $className = 'BillingSettings';
    }

    $ignoredNamespace = Arr::get($ignoredClasses, $namespace, []);

    // Skip if the class is ignored
    if (in_array($className, $ignoredNamespace)) {
        continue;
    }

    // Loop over the individual HTTP methods supported on the API path.
    foreach ($methods as $verb => $spec) {
        // Summary will be used in the docblock
        $summary = $spec['summary'];

        // The operation ID is used in the docblock and to determine the method name
        $operationId = $spec['operationId'];
        $opId = explode('#', $spec['operationId']);

        // Extract the method name
        $methodName = Arr::last($opId);

        // Determine the path to the data file

        // Uppercase of the HTTP verb for the docblock
        $upperVerb = strtoupper($verb);

        // Determine if there is an ID parameter in the URL
        $pathHasId = Str::contains($path, '{id}');

        // Use "get" as the method name and the entire base path if this is an index operation
        if ($methodName === 'index') {
            $methodName = 'get';
            $guzzlePath = '%s.json';
        } else if ($methodName === 'create') {
            $guzzlePath = '%s.json';
        } else {
            $guzzlePath = ltrim(str_replace($basePath, '%s', str_replace('{id}', '%d', $path)), '/');
        }

        // Skip update methods that are missing the resource ID from the path
        if ($methodName === 'update' && !$pathHasId) {
            continue;
        }

        $testData = getResponses($spec['responses']);

        foreach ($testData as $code => $data) {
            $dataPath = sprintf('tests/data/%s/%s/%s_%d.json', $namespace, $className, $methodName, $code);

            $dataDir = pathinfo($dataPath, PATHINFO_DIRNAME);

            if (!is_dir($dataDir)) {
                mkdir($dataDir, 0775, true);
            }

            file_put_contents($dataPath, json_encode($data, JSON_PRETTY_PRINT));
        }
    }
}

/**
 * Generate fake data to test on the mock API.
 *
 * @param Generator $faker
 * @param array $schema
 * @return array
 */
function getSourceData(Generator $faker, array $schema): array
{
    $source = [];

    foreach ($schema['properties'] as $k => $v) {
        $type = Arr::get($v, 'type', '');

        if ($k === 'created_at' || $k === 'updated_at') {
            continue;
        }

        if ($type === 'string') {
            if (Arr::has($v, 'enum') && is_array($v['enum'])) {
                $source[$k] = $faker->randomElement($v['enum']);
            } else if (Arr::has($v, 'format') && ($v['format'] === 'date-time' || $v['format'] === 'date')) {
                $source[$k] = $faker->iso8601();
            } else if ($k === 'phone_number') {
                $source[$k] = $faker->phoneNumber();
            } else if ($k === 'name') {
                $source[$k] = $faker->name();
            } else if ($k === 'first_name') {
                $source[$k] = $faker->firstName();
            } else if ($k === 'last_name') {
                $source[$k] = $faker->lastName();
            } else if ($k === 'street') {
                $source[$k] = $faker->streetAddress();
            } else if ($k === 'city') {
                $source[$k] = $faker->city();
            } else if ($k === 'province') {
                $source[$k] = $faker->state();
            } else if ($k === 'postal_code') {
                $source[$k] = $faker->postcode();
            } else if ($k === 'country') {
                $source[$k] = $faker->country();
            } else if ($k === 'email') {
                $source[$k] = $faker->email();
            } else if ($k === 'target_url' || $k === 'url') {
                $source[$k] = $faker->url();
            } else {
                $source[$k] = $faker->realText(65);
            }
        } else if ($type === 'boolean') {
            $source[$k] = $faker->boolean();
        } else if ($type === 'integer') {
            if (Arr::has($v, 'format') && $v['format'] === 'double') {
                $source[$k] = $faker->randomFloat(2);
            } else {
                $source[$k] = $faker->randomDigitNotNull();
            }
        } else if ($type === 'object' && Arr::has($v, 'properties') && is_array($v['properties'])) {
            $source[$k] = getSourceData($faker, $v);
        } else if ($type === 'array' && Arr::has($v, 'items') && is_array($v['items'])) {
            $itemCount = $faker->numberBetween(1, 4);
            $source[$k] = [];

            for ($i = 1; $i <= $itemCount; $i++) {
                if (Arr::has($v['items'], 'properties') && is_array($v['items']['properties'])) {
                    $source[$k][] = getSourceData($faker, $v['items']);
                } else {
                    array_merge($source[$k], getSourceData($faker, [
                        'properties' => [
                            $i => $v['items']
                        ]
                    ]));
                }
            }
        }
    }

    return $source;
}

/**
 * Get a specific schema.
 *
 * @param array  $schemaSet
 * @param string $schemaName
 * @return array
 * @throws Exception
 */
function getSchema(array $schemaSet, string $schemaName): array
{
    $schemaName = str_replace('#/components/schemas/', '', $schemaName);

    if (!Arr::has($schemaSet, $schemaName)) {
        throw new Exception(sprintf('No Schema "%s"', $schemaName));
    }

    return Arr::get($schemaSet, $schemaName, []);
}

/**
 * Replace all $ref keys with the actual schema, and flatten the array up one level.
 *
 * @param array $schemaSet
 * @param array $schema
 * @return array
 * @throws Exception
 */
function replaceSchemaLinks(array $schemaSet, array $schema): array
{
    foreach ($schema as $k => $v) {
        if ($k === '$ref') {
            $schema = replaceSchemaLinks($schemaSet, getSchema($schemaSet, $v));
            break;
        } else if (is_array($v)) {
            if (Arr::has($v, '$ref')) {
                $schema[$k] = replaceSchemaLinks($schemaSet, getSchema($schemaSet, $v['$ref']));
            } else if ($k === 'allOf' || $k === 'anyOf' || $k === 'oneOf') {
                $converged = [];

                foreach ($v as $subSchema) {
                    $subSchema = replaceSchemaLinks($schemaSet, $subSchema);
                    $converged = array_merge_recursive($converged, $subSchema);
                }

                // Flatten the type key if it's a single value.
                if (Arr::has($converged, 'type') && is_array($converged['type'])) {
                    $converged['type'] = array_unique($converged['type']);

                    if (count($converged['type']) === 1) {
                        $converged['type'] = $converged['type'][0];
                    }
                }

                $schema = $converged;
                break;
            } else {
                $schema[$k] = replaceSchemaLinks($schemaSet, $v);
            }
        }
    }

    return $schema;
}

/**
 * Get the schemas for all response codes.
 *
 * @param array $responses
 * @return array
 */
function getResponses(array $responses): array
{
    $schemaSet = [];
    $apiSpec = json_decode(file_get_contents('clio_swagger.json'), true);
    $apiSchemas = Arr::get($apiSpec, 'components.schemas', []);
    $faker = Faker\Factory::create();

    foreach ($responses as $code => $response) {
        try {
            $schema = getSchema($apiSchemas, Arr::get($response, 'content.application/json.schema.$ref', ''));
            $finalSchema = replaceSchemaLinks($apiSchemas, $schema);

            $schemaSet[$code] = getSourceData($faker, $finalSchema);
        } catch (Exception $e) {
            echo sprintf("Failed to generate response for code %s.\n%s\n", $code, $e->getMessage());
        }
    }

    return $schemaSet;
}
<?php

return [
    'app_key'    => env('CLIO_APP_KEY', ''),
    'app_secret' => env('CLIO_APP_SECRET', '')
];
<?php
require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Support\Str;
use Illuminate\Support\Arr;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

// Load the Swagger API Spec
$apiSpec = json_decode(file_get_contents('clio_swagger.json'), true);

// Get the paths supported by the API
$paths = Arr::get($apiSpec, 'paths', []);

// Get the schemas that are expected in the responses
$apiSchemas = Arr::get($apiSpec, 'components.schemas', []);

// Build a lookup dictionary of classes as keys, and the namespace they belong to as values.
$xTags = Arr::get($apiSpec, 'x-tagGroups', []);
$namespaces = [];

foreach ($xTags as $xTag) {
    $xTagTags = Arr::get($xTag, 'tags', []);

    foreach ($xTagTags as $xTagTag) {
        $xTagTag = Str::singular(preg_replace('/[^A-Za-z0-9]/', '', $xTagTag));
        $namespaces[$xTagTag] = preg_replace('/[^A-Za-z0-9]/', '', $xTag['name']);
    }
}

$phpunit = new SimpleXMLElement('<phpunit></phpunit>');
$phpunit->addAttribute('backupGlobals', 'false');
$phpunit->addAttribute('backupStaticAttributes', 'false');
$phpunit->addAttribute('bootstrap', 'vendor/autoload.php');
$phpunit->addAttribute('colors', 'true');
$phpunit->addAttribute('convertErrorsToExceptions', 'true');
$phpunit->addAttribute('convertNoticesToExceptions', 'true');
$phpunit->addAttribute('convertWarningsToExceptions', 'true');
$phpunit->addAttribute('processIsolation', 'false');
$phpunit->addAttribute('stopOnFailure', 'false');

$testSuites = $phpunit->addChild('testsuites');
$suites = [];

foreach ($paths as $path => $methods) {
    // Normally the base path will be the directory name of the path
    $basePath = ltrim(pathinfo($path, PATHINFO_DIRNAME), '/');

    // If the base path directory name is empty, use the filename of the path (without the extension)
    if (empty($basePath)) {
        $basePath = ltrim(pathinfo($path, PATHINFO_FILENAME), '/');
    }

    // Get the first HTTP method spec from the path
    $firstMethod = Arr::first($methods);

    // Get the tags of the first HTTP method
    $firstMethodTags = Arr::get($firstMethod, 'tags');

    // The class name we expect to use will be the first tag provided.
    // Converted to StudlyCase and non-alphanumeric characters are removed.
    $className = preg_replace('/[^A-Za-z0-9]/', '', Str::singular(Str::studly(Arr::first($firstMethodTags))));

    // Lookup the namespace. If it's not found, use the class name.
    $namespace = Arr::has($namespaces, $className) ? $namespaces[$className] : Str::studly(Arr::first($firstMethodTags));

    if ($className === 'BillingSetting') {
        $className = 'BillingSettings';
    }

    if (!array_key_exists($namespace, $suites)) {
        $suites[$namespace] = [];
    }

    $suites[$namespace][] = sprintf('tests/%s/%sTest.php', $namespace, $className);

}

foreach ($suites as $namespace => $files) {
    $files = array_unique($files);
    $testSuite = $testSuites->addChild('testsuite');
    $testSuite->addAttribute('name', strtolower($namespace));

    foreach ($files as $path) {
        $testSuite->addChild('file', $path);
    }
}

$phpunit->asXML('phpunit.xml');
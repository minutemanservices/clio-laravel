<?php
require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Support\Str;
use Illuminate\Support\Arr;

// Load the Swagger API Spec
$apiSpec = json_decode(file_get_contents('clio_swagger.json'), true);

// Get the paths supported by the API
$paths = Arr::get($apiSpec, 'paths', []);

// Build a lookup dictionary of classes as keys, and the namespace they belong to as values.
$xTags = Arr::get($apiSpec, 'x-tagGroups', []);
$namespaces = [];

foreach ($xTags as $xTag) {
    $xTagTags = Arr::get($xTag, 'tags', []);

    foreach ($xTagTags as $xTagTag) {
        $xTagTag = Str::singular(preg_replace('/[^A-Za-z0-9]/', '', $xTagTag));
        $namespaces[$xTagTag] = preg_replace('/[^A-Za-z0-9]/', '', $xTag['name']);
    }
}

// These classes will be skipped (i.e. the class is already working and we don't want to overwrite)
// Formatted as: [Namespace => [Class Name, Class Name]]
$ignoredClasses = [
//    'Api' => [
//        'CustomActionsBeta'
//    ],
//    'CourtRules' => [
//        'JurisdictionsToTriggers'
//    ],
//    'Documents' => [
//        'Documents',
//        'DocumentArchives',
//        'DocumentAutomations',
//        'DocumentCategories',
//        'DocumentTemplates',
//        'Folders'
//    ]
];

$skipClasses = true;

// This will contain the content of each class file, keyed by the path to the file
$classDocs = [];

// Loop over each API path
foreach ($paths as $path => $methods) {
    // Normally the base path will be the directory name of the path
    $basePath = ltrim(pathinfo($path, PATHINFO_DIRNAME), '/');

    // If the base path directory name is empty, use the filename of the path (without the extension)
    if (empty($basePath)) {
        $basePath = ltrim(pathinfo($path, PATHINFO_FILENAME), '/');
    }

    // Get the first HTTP method spec from the path
    $firstMethod = Arr::first($methods);

    // Get the tags of the first HTTP method
    $firstMethodTags = Arr::get($firstMethod, 'tags');

    // The class name we expect to use will be the first tag provided.
    // Converted to StudlyCase and non-alphanumeric characters are removed.
    $className = preg_replace('/[^A-Za-z0-9]/', '', Str::singular(Str::studly(Arr::first($firstMethodTags))));

    // Lookup the namespace. If it's not found, use the class name.
    $namespace = Arr::has($namespaces, $className) ? $namespaces[$className] : Str::studly(Arr::first($firstMethodTags));

    $ignoredNamespace = Arr::get($ignoredClasses, $namespace, []);

    // Skip if the class is ignored
    if (in_array($className, $ignoredNamespace)) {
        continue;
    }

    // Format the package string for the namespace and docblock of the class
    $package = sprintf('MinuteMan\\Clio\\Resources\\%s', $namespace);

    // Determine the path to the class file
    $classPath = sprintf('src/Resources/%s/%s.php', $namespace, $className);

    // Setup the opening content of the class if it's not already there.
    if (!Arr::has($classDocs, $classPath)) {
        $classDocs[$classPath] = <<<EOT
<?php

namespace $package;

use MinuteMan\Clio\Resources\Base;

/**
 * Class $className
 *
 * @package $package
 */
class $className extends Base
{

    /**
     * @var string
     */
    public static \$basePath = '$basePath';

EOT;
    }

    // Loop over the individual HTTP methods supported on the API path.
    foreach ($methods as $verb => $spec) {
        // Summary will be used in the docblock
        $summary = $spec['summary'];

        // The operation ID is used in the docblock and to determine the method name
        $operationId = $spec['operationId'];
        $opId = explode('#', $spec['operationId']);

        // Extract the method name
        $methodName = Arr::last($opId);

        // Uppercase of the HTTP verb for the docblock
        $upperVerb = strtoupper($verb);

        // Determine if there is an ID parameter in the URL
        $pathHasId = Str::contains($path, '{id}');

        // Use "get" as the method name and the entire base path if this is an index operation
        if ($methodName === 'index') {
            $methodName = 'get';
            $guzzlePath = '%s.json';
        } else if ($methodName === 'create') {
            $guzzlePath = '%s.json';
        } else {
            $guzzlePath = ltrim(str_replace($basePath, '%s', str_replace('{id}', '%d', $path)), '/');
        }

        // Skip update methods that are missing the resource ID from the path
        if ($methodName === 'update' && !$pathHasId) {
            continue;
        }

        // Return type declaration string
        $casts = '';

        // Arguments for the method declaration
        $args = [];

        // Arguments to be listed in the docblock
        $docArgs = [];

        // Variables to include with the formatted URL that gets passed to Guzzle
        $urlArgs = [
            'static::$basePath'
        ];

        // Add the $id argument if there is an ID parameter in the path.
        if ($pathHasId) {
            $docArgs[] = '@param $id';
            $args[] = '$id';
            $urlArgs[] = '$id';
        }

        // Create or Update requests will use a $data array argument and a $fields argument to define which fields are returned in the response
        if ($methodName === 'update' || $methodName === 'create') {
            $docArgs[] = '@param array $data';
            $args[] = 'array $data = []';
            $docArgs[] = '@param null|string|array $fields';
            $args[] = 'array $fields = null';
        }

        // If the method is "destroy" and there is a path ID, the method will only return a boolean result.
        if ($methodName === 'destroy' && $pathHasId) {
            $casts = ': bool';
        }

        // This will contain arguments to pass to the Guzzle request method
        $guzzleArgs = [
            sprintf('sprintf(\'$guzzlePath\', %s)', implode(', ', $urlArgs))
        ];

        // Most requests will support an arbitrary parameters array.
        // Delete, Create, and Update requests, or request with a ID parameter will not support arbitrary parameters.
        if ($methodName !== 'destroy' && !$pathHasId && $methodName !== 'update' && $methodName !== 'create') {
            $docArgs[] = '@param array $params';
            $args[] = 'array $params = []';
            $guzzleArgs[] = '[\'query\' => $params]';
        }

        // If method arguments exist, create a string to append to the docblock
        $docArgs = !empty($docArgs) ? "\n     * " . implode("\n     * ", $docArgs) : '';

        // Comma-delimit the arguments for the method declaration
        $args = implode(', ', $args);

        // Comma-delimit the arguments for the Guzzle request method
        $guzzleArgs = implode(', ', $guzzleArgs);

        // Setup the opening code for the method
        $classDocs[$classPath] .= <<<EOT
        
    /**
     * $summary
     * Method: $upperVerb
     * Path: $path
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/$operationId$docArgs
     * @return mixed
     */
    public function $methodName($args)$casts
    {
EOT;

        // Create and Update methods pass the $data argument to the Guzzle form_data parameter.
        if ($verb === 'patch' || $verb === 'post') {
            $classDocs[$classPath] .= <<<EOT
            
        \$params = [
            'form_params' => [
                'data' => \$data
            ]
        ];

        // Add fields if provided
        if (!empty(\$fields)) {
            if (is_string(\$fields)) {
                \$params['query'] = \$fields;
            } else if (is_array(\$fields)) {
                \$params['query'] = implode(',', \$fields);
            }
        }
        
EOT;

        }

        // Add the code for the Guzzle request method and response.
        $classDocs[$classPath] .= <<<EOT
        
        \$response = \$this->client->$verb($guzzleArgs);

        return \$this->fromJson(\$response->getBody());
    }
    
EOT;
    }
}

// Now that the class code is completed, add the closing brace and write the files.
if (!$skipClasses) {
    foreach ($classDocs as $path => $content) {
        $dir = pathinfo($path, PATHINFO_DIRNAME);

        if (!is_dir($dir)) {
            mkdir($dir, 0775, true);
        }

        $content .= <<<EOT

}
EOT;
        file_put_contents(__DIR__ . '/' . $path, $content);
    }
}
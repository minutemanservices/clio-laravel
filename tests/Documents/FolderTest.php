<?php

namespace MinuteMan\Clio\Tests\Documents;

use GuzzleHttp\Exception\RequestException;
use MinuteMan\Clio\Tests\Base;

/**
 * Class FolderTest
 *
 * @package MinuteMan\Clio\Tests\Documents
 */
class FolderTest extends Base
{

    /**
     * Test whether or not a list of folders can be retrieved.
     */
    public function testGetSuccess()
    {
        $folder = $this->getTestResource('Documents', 'Folder', 'get', 200);
        $result = $folder->get();

        $this->assertIsArray($result);
        $this->assertArrayHasKey('data', $result);
        $this->assertIsArray($result['data']);
    }

    /**
     * Test for error objects in the responses of any requests that return a 400, 401, 403, or 429 status code.
     */
    public function testGetErrors()
    {
        $codes = [400, 401, 403, 429];

        foreach ($codes as $code) {
            try {
                $folder = $this->getTestResource('Documents', 'Folder', 'get', $code);
                $folder->get();
            } catch (RequestException $e) {
                $error = $this->fromJson($e->getResponse()->getBody());
                $this->assertIsArray($error);
                $this->assertArrayHasKey('error', $error);
                $this->assertIsArray($error['error']);
                $this->assertArrayHasKey('type', $error['error']);
                $this->assertArrayHasKey('message', $error['error']);
            }
        }
    }

    /**
     * Test whether or not a folder can be created.
     */
    public function testCreateSuccess()
    {
        $folder = $this->getTestResource('Documents', 'Folder', 'create', 201);
        $parentId = $this->faker()->randomDigitNotNull();
        $name = $this->faker()->realText('20');

        $result = $folder->create([
            'name'   => $name,
            'parent' => [
                'type' => 'Folder',
                'id'   => $parentId
            ]
        ]);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('data', $result);
        $this->assertIsArray($result['data']);
        $this->assertArrayHasKey('id', $result['data']);
    }

    /**
     * Test for error objects in the responses of any requests that return a 400, 401, 403, 404, 422, or 429 status code.
     */
    public function testCreateErrors()
    {
        $codes = [400, 401, 403, 404, 422, 429];

        foreach ($codes as $code) {
            try {
                $folder = $this->getTestResource('Documents', 'Folder', 'create', $code);
                $folder->get();
            } catch (RequestException $e) {
                $error = $this->fromJson($e->getResponse()->getBody());
                $this->assertIsArray($error);
                $this->assertArrayHasKey('error', $error);
                $this->assertIsArray($error['error']);
                $this->assertArrayHasKey('type', $error['error']);
                $this->assertArrayHasKey('message', $error['error']);
            }
        }
    }

}
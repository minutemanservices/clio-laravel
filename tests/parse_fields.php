<?php

use MinuteMan\Clio\ResourceFieldset;

require_once __DIR__ . '/../vendor/autoload.php';


$fields = new ResourceFieldset();
$fields->addField('id');
$fields->addField('latest_document_version{uuid,put_url,put_headers}');

print_r($fields->__toString());
echo "\n";
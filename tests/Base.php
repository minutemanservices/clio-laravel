<?php

namespace MinuteMan\Clio\Tests;

use Faker\Factory as Faker;
use Faker\Generator as FakerGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Class Base
 *
 * @package MinuteMan\Clio\Tests
 */
abstract class Base extends TestCase
{

    /**
     * @var FakerGenerator
     */
    protected $faker;

    /**
     * Decode JSON data. Defaults to an associative array.
     *
     * @param      $encodedData
     * @param bool $asObject
     * @return mixed
     */
    protected function fromJson($encodedData, $asObject = false)
    {
        return json_decode($encodedData, !$asObject);
    }

    /**
     * @return FakerGenerator
     */
    protected function faker()
    {
        if (!$this->faker instanceof FakerGenerator) {
            $this->faker = Faker::create();
        }

        return $this->faker;
    }

    /**
     * Retrieve the response data for a particular resource, method, and status code.
     *
     * @param string $namespace
     * @param string $resource
     * @param string $method
     * @param        $statusCode
     * @return bool|mixed
     */
    protected function getResponseData(string $namespace, string $resource, string $method, $statusCode)
    {
        $namespace = Str::pluralStudly($namespace);
        $resource = Str::studly($resource);
        $path = sprintf('%s/data/%s/%s/%s_%s.json', __DIR__, $namespace, $resource, $method, $statusCode);

        if (file_exists($path)) {
            return file_get_contents($path);
        } else {
            return false;
        }
    }

    /**
     * Creates a Guzzle client for a particular request.
     *
     * @param string $namespace
     * @param string $resource
     * @param string $method
     * @param        $statusCode
     * @return Client
     */
    protected function guzzle(string $namespace, string $resource, string $method, $statusCode)
    {
        $namespace = Str::pluralStudly($namespace);
        $resource = Str::studly($resource);
        $responseData = $this->getResponseData($namespace, $resource, $method, $statusCode);

        // Create a mock and queue the response
        $mock = new MockHandler([
            new Response($statusCode, [], $responseData)
        ]);

        return new Client(['handler' => HandlerStack::create($mock)]);
    }

    /**
     * Retrieving a resource instance and provide a mock Guzzle Client.
     *
     * @param string $namespace
     * @param string $resource
     * @param string $method
     * @param        $statusCode
     * @return \MinuteMan\Clio\Resources\Base
     */
    public function getTestResource(string $namespace, string $resource, string $method, $statusCode)
    {
        $namespace = Str::pluralStudly($namespace);
        $resource = Str::studly($resource);
        $client = $this->guzzle($namespace, $resource, $method, $statusCode);
        $className = sprintf('MinuteMan\\Clio\\Resources\\%s\\%s', $namespace, $resource);

        if (class_exists($className)) {
            return new $className($client);
        } else {
            throw new InvalidArgumentException(sprintf('No class found for "%s"', $className));
        }
    }

}
<?php

use Dotenv\Dotenv;
use MinuteMan\Clio\HttpClient;

require_once __DIR__ . '/../vendor/autoload.php';

(Dotenv::create(sprintf('%s/../', __DIR__)))->load();

$client = new HttpClient(
    env('CLIO_APP_KEY'),
    env('CLIO_APP_SECRET'),
    env('CLIO_TOKEN'),
    env('CLIO_REFRESH_TOKEN')
);

$folder = $client->documents->folder;

print_r($folder->get(['fields' => 'id,etag,name,type,parent,creator,document_category,created_at,updated_at,deleted_at,external_properties,latest_document_version,contact,matter']));
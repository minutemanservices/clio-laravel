<?php

use Dotenv\Dotenv;
use MinuteMan\Clio\HttpClient;

require_once __DIR__ . '/../vendor/autoload.php';

(Dotenv::create(sprintf('%s/../', __DIR__)))->load();

$client = new HttpClient(
    env('CLIO_APP_KEY'),
    env('CLIO_APP_SECRET'),
    env('CLIO_TOKEN'),
    env('CLIO_REFRESH_TOKEN')
);

$folder = $client->documents->folder;

$rootFolders = $folder->get(['fields' => 'id,type', 'query' => 'root', 'limit' => 1]);
$rootFolder = false;

if (
    is_array($rootFolders) &&
    array_key_exists('data', $rootFolders) &&
    is_array($rootFolders['data']) &&
    !empty($rootFolders['data']) &&
    isset($rootFolders['data'][0])
) {
    $rootFolder = $rootFolders['data'][0];
}

if (
    !empty($rootFolder) &&
    is_array($rootFolder) &&
    array_key_exists('id', $rootFolder) &&
    array_key_exists('type', $rootFolder)
) {
    echo sprintf("Found root folder (#%d).\n", $rootFolder['id']);

    $newFolder = $folder->create([
        'name'   => 'Test Folder',
        'parent' => $rootFolder
    ]);

    print_r($newFolder);

    if (
        is_array($newFolder) &&
        array_key_exists('data', $newFolder) &&
        is_array($newFolder['data']) &&
        array_key_exists('id', $newFolder['data'])
    ) {
        echo sprintf("Created test folder (#%d).\n", $newFolder['data']['id']);

        $isDeleted = $folder->destroy($newFolder['data']['id']);

        if ($isDeleted) {
            echo sprintf("Deleted test folder (#%d).\n", $newFolder['data']['id']);
        }
    }
} else {
    echo "Could not locate root folder.\n";
}
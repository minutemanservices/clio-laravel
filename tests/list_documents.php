<?php

use Dotenv\Dotenv;
use MinuteMan\Clio\HttpClient;

require_once __DIR__ . '/../vendor/autoload.php';

(Dotenv::create(sprintf('%s/../', __DIR__)))->load();

$client = new HttpClient(
    env('CLIO_APP_KEY'),
    env('CLIO_APP_SECRET'),
    env('CLIO_TOKEN'),
    env('CLIO_REFRESH_TOKEN')
);

$document = $client->documents->document;

print_r($document->get(['fields' => 'id,etag,name']));
<?php

namespace MinuteMan\Clio;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Str;
use InvalidArgumentException;
use kamermans\OAuth2\GrantType\RefreshToken;
use kamermans\OAuth2\OAuth2Middleware;
use MinuteMan\Clio\Resources\Base;

class HttpClient
{

    const PROTOCOL = 'https';
    const DOMAIN = 'app.clio.com';
    const OAUTH_PATH = '/oauth';
    const API_PATH = '/api/v4/';

    /**
     * @var string
     */
    protected $appKey = '';

    /**
     * @var string
     */
    protected $appSecret = '';

    /**
     * @var string
     */
    protected $token = '';

    /**
     * @var string
     */
    protected $refreshToken = '';

    /**
     * @var Client
     */
    protected $guzzle;

    /**
     * HttpClient constructor.
     *
     * @param string      $appKey
     * @param string      $appSecret
     * @param null|string $token
     * @param null|string $refreshToken
     */
    public function __construct(string $appKey, string $appSecret, $token = null, $refreshToken = null)
    {
        $this->appKey = $appKey;
        $this->appSecret = $appSecret;
        $this->token = $token;
        $this->refreshToken = $refreshToken;
    }

    /**
     * Returns a URL to redirect the user in order to get an authorization code.
     *
     * @param string|null $redirectUrl
     * @return string
     */
    public function getAuthorizationUrl($redirectUrl = null): string
    {
        $params = [
            'response_type' => 'code',
            'client_id'     => $this->appKey,
            'redirect_uri'  => $redirectUrl ?? $this->redirectUri
        ];

        return sprintf('%s://%s%s/authorize?%s', self::PROTOCOL, self::DOMAIN, self::OAUTH_PATH, http_build_query($params));
    }

    /**
     * Get the instance of the Guzzle Client.
     *
     * @return Client
     */
    public function guzzle()
    {
        if (!$this->guzzle instanceof Client) {
            $grantClient = new Client([
                'base_uri' => sprintf('%s://%s%s/token', self::PROTOCOL, self::DOMAIN, self::OAUTH_PATH)
            ]);

            $grant = new RefreshToken($grantClient, [
                'client_id'     => $this->appKey,
                'client_secret' => $this->appSecret,
                'refresh_token' => $this->refreshToken
            ]);

            $oauth = new OAuth2Middleware($grant);

            $stack = HandlerStack::create();
            $stack->push($oauth);

            $this->guzzle = new Client([
                'auth'     => 'oauth',
                'handler'  => $stack,
                'base_uri' => sprintf('%s://%s%s', self::PROTOCOL, self::DOMAIN, self::API_PATH)
            ]);
        }

        return $this->guzzle;
    }

    /**
     * Magic method for retrieving resource instances.
     * Generates an anonymous class with the requested namespace.
     * The anonymous class is then used to retrieve specific resources.
     * Instantiates resource objects with this instance as the client.
     *
     * Usage: $this->communications->conversation_message returns an instance of MinuteMan\Clio\Resources\Communications\ConversationMessage
     *
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        } else {
            return new class (Str::studly($key), $this->guzzle())
            {
                /**
                 * @var string
                 */
                protected $namespace = '';

                /**
                 * @var Client
                 */
                protected $client;

                /**
                 * Constructor for the resource generating class.
                 * This will instantiate classes within a particular namespace.
                 *
                 * @param string $namespace
                 * @param Client $client
                 */
                public function __construct(string $namespace, Client $client)
                {
                    $this->namespace = $namespace;
                    $this->client = $client;
                }

                /**
                 * Magic method for retrieving resource instances.
                 * Instantiates resource objects with this instance as the client.
                 *
                 * @param $key
                 * @return Base|bool|mixed
                 */
                public function __get($key)
                {
                    $className = sprintf('%s\\Resources\\%s\\%s', __NAMESPACE__, $this->namespace, Str::studly($key));

                    if (property_exists($this, $key)) {
                        return $this->$key;
                    } else if (class_exists($className)) {
                        return new $className($this->client);
                    } else {
                        throw new InvalidArgumentException(sprintf('No class found for "%s"', $className));
                    }
                }
            };
        }
    }

}
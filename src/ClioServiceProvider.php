<?php

namespace MinuteMan\Clio;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class ClioServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services, routes, and config.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/clio.php' => config_path('clio.php')
        ], 'config');

        $this->app->bind('ClioClient', function ($app, array $params = []) {
            $authorizationCode = Arr::get($params, 'code', null);

            return new HttpClient(
                Config::get('clio.app_key'),
                Config::get('clio.app_secret'),
                $authorizationCode
            );
        });
    }

}
<?php

namespace MinuteMan\Clio;

/**
 * Class ResourceFieldset
 *
 * @package MinuteMan\Clio
 */
class ResourceFieldset
{

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var array
     */
    protected $nestedFields = [];

    /**
     * ResourceFieldset constructor.
     *
     * @param string|array|null|ResourceFieldset $fieldInput
     */
    public function __construct($fieldInput = null)
    {
        if (is_string($fieldInput) && !empty($fieldInput)) {
            $this->parseString(($fieldInput));
        } else if (is_array($fieldInput) && !empty($fieldInput)) {
            $this->parseArray($fieldInput);
        } else if ($fieldInput instanceof ResourceFieldset) {
            $this->parseString($fieldInput->__toString());
        }
    }

    /**
     * Returns whether or not there are fields added to the fieldset.
     *
     * @return bool
     */
    public function hasFields(): bool
    {
        return !empty($this->fields);
    }

    /**
     * Add a field to the fieldset.
     * If a parent name is provided, the field is added as a nested field.
     *
     * @param string      $fieldName
     * @param string|null $parentName
     * @return $this
     */
    public function addField(string $fieldName, string $parentName = null)
    {
        if (is_string($parentName) && !empty($parentName)) {
            $this->parseString(sprintf('%s{%s}', $parentName, $fieldName));
        } else if (is_string($fieldName)) {
            $this->parseString($fieldName);
        }

        return $this;
    }

    /**
     * Remove a field from the fieldset.
     * Provide a parent name to remove a nested field from that parent field.
     * If a $fieldName also has nested fields, those are removed too.
     *
     * @param string      $fieldName
     * @param string|null $parentName
     * @return $this
     */
    public function removeField(string $fieldName, string $parentName = null)
    {
        if (is_string($parentName) && !empty($parentName)) {
            // If the parent fieldset exists, remove it from the nested fields
            if (array_key_exists($parentName, $this->nestedFields) && is_array($this->nestedFields[$parentName])) {
                $index = array_search($fieldName, $this->nestedFields[$parentName]);

                if ($index !== false) {
                    unset($this->nestedFields[$parentName][$index]);
                }
            }

            if (empty($this->nestedFields[$parentName])) {
                unset($this->nestedFields[$parentName]);
            }
        } else if (in_array($fieldName, $this->fields)) {
            $index = array_search($fieldName, $this->fields);

            if ($index !== false) {
                unset($this->fields[$index]);
            }

            // If the field is removed and has nested fields, remove the nested fields too.
            if (array_key_exists($fieldName, $this->nestedFields)) {
                unset($this->nestedFields[$parentName]);
            }
        }

        return $this;
    }

    /**
     * Determine if the field exists in $fields, or in $nestedFields if a parent field name is provided.
     *
     * @param string      $fieldName
     * @param string|null $parentName
     * @return bool
     */
    public function hasField(string $fieldName, string $parentName = null): bool
    {
        if (is_string($parentName) && !empty($parentName)) {
            return (
                array_key_exists($parentName, $this->nestedFields) &&
                is_array($this->nestedFields[$parentName]) &&
                in_array($fieldName, $this->nestedFields[$parentName])
            );
        } else {
            return in_array($fieldName, $this->fields);
        }
    }

    /**
     * Assemble the fields and nested fields from an array (possibly multidimensional).
     *
     * @param array $fields
     */
    protected function parseArray(array $fields)
    {
        foreach ($fields as $k => $v) {
            // If we encounter an array, use the key as the field name and iterate over the values as nested field names.
            if (is_array($v) && is_string($k)) {
                array_push($this->fields, $k);

                // Use the value as the nested fields
                $this->nestedFields[$k] = array_unique(array_values(array_filter($v, 'is_string')));
            } else if (is_string($v) && !in_array($v, $this->fields)) {
                array_push($this->fields, $v);
            }
        }
    }

    /**
     * Assemble the fields and nested fields from a comma delimited string of fields.
     * Refer to the Clio API documentation for the format of the field string.
     *
     * @link https://app.clio.com/api/v4/documentation?#section/Fields
     * @param string $fields
     */
    protected function parseString(string $fields)
    {
        $chars = str_split($fields);
        $fieldName = '';
        $nestedFieldName = '';

        foreach ($chars as $char) {
            if ($char === ',' && empty($nestedFieldName)) {
                if (!empty($fieldName) && !in_array($fieldName, $this->fields)) {
                    array_push($this->fields, $fieldName);
                }

                // Reset to a new field
                $fieldName = '';
            } else if ($char === ',' && !empty($nestedFieldName)) {
                // If a field delimiter is encountered and there is a nested field name assigned,
                // add the field to the $nestedFields property under the name assigned to $nestedField.
                if (!empty($fieldName) && !in_array($fieldName, $this->nestedFields[$nestedFieldName])) {
                    array_push($this->nestedFields[$nestedFieldName], $fieldName);
                }

                // Reset to a new field
                $fieldName = '';
            } else if ($char === '{') {
                if (!empty($fieldName) && !in_array($fieldName, $this->fields)) {
                    array_push($this->fields, $fieldName);
                }

                // If there isn't already an array to contain nested fields under the current field name, create it
                if (!array_key_exists($fieldName, $this->nestedFields)) {
                    $this->nestedFields[$fieldName] = [];
                }

                // Assign the nestedFieldName so we know not to store the subsequent fields in the $fields property.
                $nestedFieldName = $fieldName;

                // Reset to a new field
                $fieldName = '';
            } else if ($char === '}') {
                if (!empty($fieldName) && !in_array($fieldName, $this->nestedFields[$nestedFieldName])) {
                    array_push($this->nestedFields[$nestedFieldName], $fieldName);
                }

                // At the end of the nested fieldset, clear the current nested field name
                $nestedFieldName = '';

                // Reset to a new field
                $fieldName = '';
            } else {
                $fieldName .= $char;
            }
        }

        // If we exit the loop and there is still a field name, make sure it's added
        if (!empty($fieldName) && !in_array($fieldName, $this->fields)) {
            array_push($this->fields, $fieldName);
        }
    }

    /**
     * Returns the comma delimited set of fields with nested fields included within braces.
     *
     * @return string
     */
    public function __toString()
    {
        return implode(',', array_map(function ($field) {
            if (
                array_key_exists($field, $this->nestedFields) &&
                is_array($this->nestedFields[$field]) &&
                !empty($this->nestedFields[$field])
            ) {
                return sprintf('%s{%s}', $field, implode(',', $this->nestedFields[$field]));
            } else {
                return $field;
            }
        }, $this->fields));
    }

}
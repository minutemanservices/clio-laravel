<?php

namespace MinuteMan\Clio\Resources\Trust;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TrustLineItems
 *
 * @package MinuteMan\Clio\Resources\Trust
 */
class TrustLineItem extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'trust_line_items';
        
    /**
     * Return the data for all TrustLineItems
     * Method: GET
     * Path: /trust_line_items.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TrustLineItem#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single TrustLineItem
     * Method: PATCH
     * Path: /trust_line_items/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TrustLineItem#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Trust;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TrustRequests
 *
 * @package MinuteMan\Clio\Resources\Trust
 */
class TrustRequest extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'trust_requests';
        
    /**
     * Create a new TrustRequest
     * Method: POST
     * Path: /trust_requests.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TrustRequest#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
    
}
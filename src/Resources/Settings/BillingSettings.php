<?php

namespace MinuteMan\Clio\Resources\Settings;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BillingSettings
 *
 * @package MinuteMan\Clio\Resources\Settings
 */
class BillingSettings extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'settings/billing';
        
    /**
     * Return the billing settings
     * Method: GET
     * Path: /settings/billing.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BillingSetting#show
     * @param array $params
     * @return mixed
     */
    public function show(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
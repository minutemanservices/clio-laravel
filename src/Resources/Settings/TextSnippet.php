<?php

namespace MinuteMan\Clio\Resources\Settings;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TextSnippets
 *
 * @package MinuteMan\Clio\Resources\Settings
 */
class TextSnippet extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'settings/text_snippets';
        
    /**
     * Return the data for TextSnippets events (BETA)
     * Method: GET
     * Path: /settings/text_snippets/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TextSnippet#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return all text snippets
     * Method: GET
     * Path: /settings/text_snippets.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TextSnippet#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a text snippet
     * Method: POST
     * Path: /settings/text_snippets.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TextSnippet#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for the text snippet
     * Method: GET
     * Path: /settings/text_snippets/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TextSnippet#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a text snippet
     * Method: PATCH
     * Path: /settings/text_snippets/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TextSnippet#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Destroy a text snippet
     * Method: DELETE
     * Path: /settings/text_snippets/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TextSnippet#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Settings;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Currency
 *
 * @package MinuteMan\Clio\Resources\Settings
 */
class Currency extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'currencies';
        
    /**
     * Return the data for all Currencies
     * Method: GET
     * Path: /currencies.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Currency#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Reporting;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Reports
 *
 * @package MinuteMan\Clio\Resources\Reporting
 */
class Report extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'reports';
        
    /**
     * Download the completed Report
     * Method: GET
     * Path: /reports/{id}/download.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Report#download
     * @param $id
     * @return mixed
     */
    public function download($id)
    {        
        $response = $this->client->get(sprintf('reports/%d/download.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Report
     * Method: POST
     * Path: /reports.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Report#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Report
     * Method: GET
     * Path: /reports/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Report#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
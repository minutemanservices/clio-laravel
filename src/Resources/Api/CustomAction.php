<?php

namespace MinuteMan\Clio\Resources\Api;

use MinuteMan\Clio\Resources\Base;

/**
 * Class CustomActions
 *
 * @package MinuteMan\Clio\Resources\Api
 */
class CustomAction extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'custom_actions';

    /**
     * Return the data for all CustomActions
     * Method: GET
     * Path: /custom_actions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomAction#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Create a new CustomAction
     * Method: POST
     * Path: /custom_actions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomAction#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }

        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }

    /**
     * Return the data for a single CustomAction
     * Method: GET
     * Path: /custom_actions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomAction#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }

    /**
     * Update a single CustomAction
     * Method: PATCH
     * Path: /custom_actions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomAction#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }

        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }

    /**
     * Delete a single CustomAction
     * Method: DELETE
     * Path: /custom_actions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomAction#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }

}
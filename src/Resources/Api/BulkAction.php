<?php

namespace MinuteMan\Clio\Resources\Api;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BulkActions
 *
 * @package MinuteMan\Clio\Resources\Api
 */
class BulkAction extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'bulk_actions';
        
    /**
     * Return the data for all BulkActions
     * Method: GET
     * Path: /bulk_actions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BulkAction#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single BulkAction
     * Method: GET
     * Path: /bulk_actions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BulkAction#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single BulkAction
     * Method: PATCH
     * Path: /bulk_actions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BulkAction#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
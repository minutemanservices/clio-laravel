<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Activity
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class Activity extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'activities';
        
    /**
     * Return the data for Activities events (BETA)
     * Method: GET
     * Path: /activities/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Activity#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Activities
     * Method: GET
     * Path: /activities.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Activity#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Activity
     * Method: POST
     * Path: /activities.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Activity#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Activity
     * Method: GET
     * Path: /activities/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Activity#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Activity
     * Method: PATCH
     * Path: /activities/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Activity#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Activity
     * Method: DELETE
     * Path: /activities/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Activity#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class UtbmsCodes
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class UtbmsCode extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'utbms/codes';
        
    /**
     * Return the data for all UtbmsCodes
     * Method: GET
     * Path: /utbms/codes.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/UtbmsCode#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single UtbmsCode
     * Method: GET
     * Path: /utbms/codes/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/UtbmsCode#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
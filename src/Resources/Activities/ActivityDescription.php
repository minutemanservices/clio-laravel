<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ActivityDescriptions
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class ActivityDescription extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'activity_descriptions';
        
    /**
     * Return the data for ActivityDescriptions events (BETA)
     * Method: GET
     * Path: /activity_descriptions/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityDescription#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all ActivityDescriptions
     * Method: GET
     * Path: /activity_descriptions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityDescription#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new ActivityDescription
     * Method: POST
     * Path: /activity_descriptions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityDescription#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single ActivityDescription
     * Method: GET
     * Path: /activity_descriptions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityDescription#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single ActivityDescription
     * Method: PATCH
     * Path: /activity_descriptions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityDescription#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single ActivityDescription
     * Method: DELETE
     * Path: /activity_descriptions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityDescription#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
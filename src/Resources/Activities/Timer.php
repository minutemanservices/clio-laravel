<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Timers
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class Timer extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'timer';
        
    /**
     * Return the data for a single Timer
     * Method: GET
     * Path: /timer.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Timer#show
     * @param array $params
     * @return mixed
     */
    public function show(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Timer
     * Method: DELETE
     * Path: /timer.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Timer#destroy
     * @return mixed
     */
    public function destroy()
    {        
        $response = $this->client->delete(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Timer
     * Method: POST
     * Path: /timer.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Timer#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
    
}
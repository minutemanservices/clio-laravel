<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ActivityRates
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class ActivityRate extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'activity_rates';
        
    /**
     * Return the data for all ActivityRates
     * Method: GET
     * Path: /activity_rates.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityRate#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new ActivityRate
     * Method: POST
     * Path: /activity_rates.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityRate#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single ActivityRate
     * Method: GET
     * Path: /activity_rates/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityRate#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single ActivityRate
     * Method: PATCH
     * Path: /activity_rates/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityRate#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single ActivityRate
     * Method: DELETE
     * Path: /activity_rates/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ActivityRate#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
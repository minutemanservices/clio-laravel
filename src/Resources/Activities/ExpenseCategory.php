<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ExpenseCategory
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class ExpenseCategory extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'expense_categories';
        
    /**
     * Return the data for all ExpenseCategories
     * Method: GET
     * Path: /expense_categories.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ExpenseCategory#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new ExpenseCategory
     * Method: POST
     * Path: /expense_categories.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ExpenseCategory#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single ExpenseCategory
     * Method: GET
     * Path: /expense_categories/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ExpenseCategory#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single ExpenseCategory
     * Method: PATCH
     * Path: /expense_categories/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ExpenseCategory#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single ExpenseCategory
     * Method: DELETE
     * Path: /expense_categories/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ExpenseCategory#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
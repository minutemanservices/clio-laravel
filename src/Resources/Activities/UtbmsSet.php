<?php

namespace MinuteMan\Clio\Resources\Activities;

use MinuteMan\Clio\Resources\Base;

/**
 * Class UtbmsSets
 *
 * @package MinuteMan\Clio\Resources\Activities
 */
class UtbmsSet extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'utbms/sets';
        
    /**
     * Return the data for all the utbms sets
     * Method: GET
     * Path: /utbms/sets.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/UtbmsSet#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
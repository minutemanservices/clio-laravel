<?php

namespace MinuteMan\Clio\Resources\Accounting;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BankTransactions
 *
 * @package MinuteMan\Clio\Resources\Accounting
 */
class BankTransaction extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'bank_transactions';
        
    /**
     * Return the data for all BankTransactions
     * Method: GET
     * Path: /bank_transactions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankTransaction#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single BankTransaction
     * Method: GET
     * Path: /bank_transactions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankTransaction#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Accounting;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BankTransfers
 *
 * @package MinuteMan\Clio\Resources\Accounting
 */
class BankTransfer extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'bank_transfers';
        
    /**
     * Return the data for a single BankTransfer
     * Method: GET
     * Path: /bank_transfers/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankTransfer#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
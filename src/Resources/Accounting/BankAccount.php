<?php

namespace MinuteMan\Clio\Resources\Accounting;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BankAccounts
 *
 * @package MinuteMan\Clio\Resources\Accounting
 */
class BankAccount extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'bank_accounts';
        
    /**
     * Return the data for BankAccounts events (BETA)
     * Method: GET
     * Path: /bank_accounts/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankAccount#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all BankAccounts
     * Method: GET
     * Path: /bank_accounts.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankAccount#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new BankAccount
     * Method: POST
     * Path: /bank_accounts.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankAccount#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single BankAccount
     * Method: GET
     * Path: /bank_accounts/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankAccount#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single BankAccount
     * Method: PATCH
     * Path: /bank_accounts/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankAccount#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single BankAccount
     * Method: DELETE
     * Path: /bank_accounts/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BankAccount#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
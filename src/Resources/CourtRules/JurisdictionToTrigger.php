<?php

namespace MinuteMan\Clio\Resources\CourtRules;

use MinuteMan\Clio\Resources\Base;

/**
 * Class JurisdictionsToTriggers
 *
 * @package MinuteMan\Clio\Resources\CourtRules
 */
class JurisdictionToTrigger extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'court_rules/jurisdictions';

    /**
     * Method: GET
     * Path: /court_rules/jurisdictions/{jurisdiction_id}/triggers.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/JurisdictionsToTrigger#index
     * @param       $jurisdictionId
     * @param array $params
     * @return mixed
     */
    public function get($jurisdictionId, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%s/triggers.json', static::$basePath, $jurisdictionId), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /court_rules/jurisdictions/{jurisdiction_id}/triggers/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/JurisdictionsToTrigger#show
     * @param $jurisdictionId
     * @param $id
     * @return mixed
     */
    public function show($jurisdictionId, $id)
    {
        $response = $this->client->get(sprintf('%s/%s/triggers/%d.json', static::$basePath, $jurisdictionId, $id));

        return $this->fromJson($response->getBody());
    }

}
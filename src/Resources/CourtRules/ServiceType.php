<?php

namespace MinuteMan\Clio\Resources\CourtRules;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ServiceTypes
 *
 * @package MinuteMan\Clio\Resources\CourtRules
 */
class ServiceType extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'court_rules';
        
    /**
     * Return the data for all service types
     * Method: GET
     * Path: /court_rules/service_types.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ServiceType#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for the service type
     * Method: GET
     * Path: /court_rules/service_types/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ServiceType#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
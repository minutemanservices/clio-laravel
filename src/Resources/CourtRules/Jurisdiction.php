<?php

namespace MinuteMan\Clio\Resources\CourtRules;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Jurisdictions
 *
 * @package MinuteMan\Clio\Resources\CourtRules
 */
class Jurisdiction extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'court_rules';
        
    /**
     * Return the data for all jurisdictions
     * Method: GET
     * Path: /court_rules/jurisdictions.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Jurisdiction#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for the jurisdiction
     * Method: GET
     * Path: /court_rules/jurisdictions/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Jurisdiction#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
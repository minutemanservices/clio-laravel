<?php

namespace MinuteMan\Clio\Resources\CourtRules;

use MinuteMan\Clio\Resources\Base;

/**
 * Class MatterDockets
 *
 * @package MinuteMan\Clio\Resources\CourtRules
 */
class MatterDocket extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'court_rules/matter_dockets';
        
    /**
     * Preview calendar dates for the docket
     * Method: GET
     * Path: /court_rules/matter_dockets/preview.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/MatterDocket#preview
     * @param array $params
     * @return mixed
     */
    public function preview(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/preview.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all matter dockets
     * Method: GET
     * Path: /court_rules/matter_dockets.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/MatterDocket#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Creates a matter docket
     * Method: POST
     * Path: /court_rules/matter_dockets.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/MatterDocket#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for the matter docket
     * Method: GET
     * Path: /court_rules/matter_dockets/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/MatterDocket#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Deletes the requested matter docket
     * Method: DELETE
     * Path: /court_rules/matter_dockets/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/MatterDocket#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
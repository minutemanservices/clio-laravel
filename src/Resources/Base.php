<?php

namespace MinuteMan\Clio\Resources;

use GuzzleHttp\Client;

/**
 * Class Base
 *
 * @package MinuteMan\Clio\Resources
 */
abstract class Base
{

    /**
     * Upload files in multipart if they exceed 100MB
     */
    const UPLOAD_MULTIPART_THRESHOLD = 100000000;

    /**
     * Upload files in multipart chunks of 30MB
     */
    const UPLOAD_CHUNK_SIZE = 30000000;

    /**
     * @var string
     */
    public static $basePath = '';

    /**
     * @var Client
     */
    protected $client;

    /**
     * Base constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Override the Guzzle Client.
     *
     * @param Client $client
     * @return $this
     */
    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Retrieve the Guzzle Client.
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Decode JSON data. Defaults to an associative array.
     *
     * @param      $encodedData
     * @param bool $asObject
     * @return mixed
     */
    protected function fromJson($encodedData, $asObject = false)
    {
        return json_decode($encodedData, !$asObject);
    }

}
<?php

namespace MinuteMan\Clio\Resources\Communications;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ConversationMessages
 *
 * @package MinuteMan\Clio\Resources\Communications
 */
class ConversationMessage extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'conversation_messages';
        
    /**
     * Return the data for all ConversationMessages
     * Method: GET
     * Path: /conversation_messages.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ConversationMessage#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new ConversationMessage
     * Method: POST
     * Path: /conversation_messages.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ConversationMessage#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single ConversationMessage
     * Method: GET
     * Path: /conversation_messages/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ConversationMessage#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Communications;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Conversations
 *
 * @package MinuteMan\Clio\Resources\Communications
 */
class Conversation extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'conversations';
        
    /**
     * Return the data for all Conversations
     * Method: GET
     * Path: /conversations.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Conversation#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Conversation
     * Method: GET
     * Path: /conversations/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Conversation#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Conversation
     * Method: PATCH
     * Path: /conversations/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Conversation#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Communications;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Communications
 *
 * @package MinuteMan\Clio\Resources\Communications
 */
class Communication extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'communications';
        
    /**
     * Return the data for Communications events (BETA)
     * Method: GET
     * Path: /communications/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Communication#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Communications
     * Method: GET
     * Path: /communications.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Communication#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Communication
     * Method: POST
     * Path: /communications.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Communication#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Communication
     * Method: GET
     * Path: /communications/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Communication#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Communication
     * Method: PATCH
     * Path: /communications/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Communication#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Communication
     * Method: DELETE
     * Path: /communications/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Communication#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
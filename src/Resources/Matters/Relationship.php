<?php

namespace MinuteMan\Clio\Resources\Matters;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Relationships
 *
 * @package MinuteMan\Clio\Resources\Matters
 */
class Relationship extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'relationships';
        
    /**
     * Return the data for all Relationships
     * Method: GET
     * Path: /relationships.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Relationship#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Relationship
     * Method: POST
     * Path: /relationships.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Relationship#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Relationship
     * Method: GET
     * Path: /relationships/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Relationship#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Relationship
     * Method: PATCH
     * Path: /relationships/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Relationship#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Relationship
     * Method: DELETE
     * Path: /relationships/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Relationship#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
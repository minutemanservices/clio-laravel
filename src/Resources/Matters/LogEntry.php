<?php

namespace MinuteMan\Clio\Resources\Matters;

use MinuteMan\Clio\Resources\Base;

/**
 * Class LogEntry
 *
 * @package MinuteMan\Clio\Resources\Matters
 */
class LogEntry extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'log_entries';
        
    /**
     * Return the data for all LogEntries
     * Method: GET
     * Path: /log_entries.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/LogEntry#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
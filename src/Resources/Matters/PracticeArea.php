<?php

namespace MinuteMan\Clio\Resources\Matters;

use MinuteMan\Clio\Resources\Base;

/**
 * Class PracticeAreas
 *
 * @package MinuteMan\Clio\Resources\Matters
 */
class PracticeArea extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'practice_areas';
        
    /**
     * Return the data for PracticeAreas events (BETA)
     * Method: GET
     * Path: /practice_areas/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/PracticeArea#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all PracticeAreas
     * Method: GET
     * Path: /practice_areas.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/PracticeArea#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new PracticeArea
     * Method: POST
     * Path: /practice_areas.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/PracticeArea#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single PracticeArea
     * Method: GET
     * Path: /practice_areas/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/PracticeArea#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single PracticeArea
     * Method: PATCH
     * Path: /practice_areas/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/PracticeArea#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single PracticeArea
     * Method: DELETE
     * Path: /practice_areas/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/PracticeArea#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Matters;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Notes
 *
 * @package MinuteMan\Clio\Resources\Matters
 */
class Note extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'notes';
        
    /**
     * Return the data for Notes events (BETA)
     * Method: GET
     * Path: /notes/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Note#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Notes
     * Method: GET
     * Path: /notes.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Note#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Note
     * Method: POST
     * Path: /notes.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Note#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Note
     * Method: GET
     * Path: /notes/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Note#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Note
     * Method: PATCH
     * Path: /notes/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Note#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Note
     * Method: DELETE
     * Path: /notes/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Note#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Users;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Groups
 *
 * @package MinuteMan\Clio\Resources\Users
 */
class Group extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'groups';
        
    /**
     * Return the data for all Groups
     * Method: GET
     * Path: /groups.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Group#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Group
     * Method: POST
     * Path: /groups.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Group#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Group
     * Method: GET
     * Path: /groups/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Group#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Group
     * Method: PATCH
     * Path: /groups/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Group#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Group
     * Method: DELETE
     * Path: /groups/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Group#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Users;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Users
 *
 * @package MinuteMan\Clio\Resources\Users
 */
class User extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'users';
        
    /**
     * Return the data for the current User
     * Method: GET
     * Path: /users/who_am_i.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/User#who_am_i
     * @param array $params
     * @return mixed
     */
    public function who_am_i(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/who_am_i.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for Users events (BETA)
     * Method: GET
     * Path: /users/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/User#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Users
     * Method: GET
     * Path: /users.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/User#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single User
     * Method: GET
     * Path: /users/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/User#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
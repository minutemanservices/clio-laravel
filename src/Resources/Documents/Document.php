<?php

namespace MinuteMan\Clio\Resources\Documents;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use MinuteMan\Clio\ResourceFieldset;
use MinuteMan\Clio\Resources\Base;

/**
 * Class Documents
 *
 * @package MinuteMan\Clio\Resources\Documents
 */
class Document extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'documents';

    /**
     * Method: GET
     * Path: /documents.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#index
     * @param array $params
     * @return array
     */
    public function get(array $params = [])
    {
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /documents/{id}/download.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#download
     * @param       $id
     * @param array $params
     * @return array
     */
    public function download($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%d/download.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /documents/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#events
     * @param array $params
     * @return array
     */
    public function events(array $params = [])
    {
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: DELETE
     * Path: /documents/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#destroy
     * @param $id
     * @return bool
     */
    public function destroy($id): bool
    {
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $response->getStatusCode() === 204;
    }

    /**
     * Method: PATCH
     * Path: /documents/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#update
     * @param       $id
     * @param array $data
     * @param null  $fields
     * @return array
     * @throws Exception
     */
    public function update($id, array $data = [], $fields = null)
    {
        $fields = new ResourceFieldset($fields);
        $fields->addField('id');

        $params = [
            'form_params' => [
                'data' => $data
            ],
            'query'       => [
                'fields' => $fields->__toString()
            ]
        ];

        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id), $params);
        $responseData = $this->fromJson($response->getBody());

        // Determine if a valid path exists and if multipart or regular upload is to be used.
        if (!empty($path) && is_string($path)) {
            if (!file_exists($path)) {
                throw new Exception(sprintf('File "%s" does not exist.', $path));
            }

            // Make sure a name is set for the file
            if (array_key_exists('name', $data) && !empty($data['name'])) {
                $name = $data['name'];
            } else {
                $name = pathinfo($path, PATHINFO_BASENAME);
            }

            // Request the multipart field if we are using multipart
            // If using multipart, the first 50 parts can be sent to the API on the initial request.
            // Parts after the first 50 need to be sent in a later request.
            // if (filesize($path) > self::UPLOAD_MULTIPART_THRESHOLD) {
            //     $this->uploadMultipart($path, $name, $responseData['data']['id']);
            // }
            $this->uploadDirect($path, $name, $responseData['data']['id']);
        }

        return $this->show($id, ['fields' => $fields->__toString()]);
    }

    /**
     * Create a document and upload it in multipart format.
     * Clio API accepts 50 parts maximum per request, and chunks should be at least 10MB (we use 30MB chunks).
     *
     * @param string          $path
     * @param string          $name
     * @param int|string|null $id
     * @param array           $formData
     * @return array
     * @throws Exception
     */
    protected function uploadMultipart(string $path, string $name, $id = null, array $formData = [])
    {
        // Request multipart fields in the response
        $fields = new ResourceFieldset();
        $fields->addField('id');
        $fields->addField('latest_document_version{uuid,fully_uploaded,put_headers,multiparts}');

        $formParams = [
            'data' => [
                'name' => $name
            ]
        ];

        if (!empty($formData)) {
            $formParams = array_merge($formParams, $formData);
        }

        if (!empty($id)) {
            $formParams['data']['parent'] = [
                'id'   => $id,
                'type' => 'Document'
            ];
        }

        // Form data to request an upload URL
        $params = [
            'form_params' => $formParams,
            'query'       => [
                'fields' => $fields->__toString()
            ]
        ];

        $params['form_params']['data']['multiparts'] = [];

        // Chunk the file
        $chunks = $this->createFileChunks($path);

        // Start with the first 50 chunks in the initial request.
        $initialChunks = array_slice($chunks, 0, 50);

        foreach ($initialChunks as $i => $chunk) {
            array_push($params['form_params']['data']['multiparts'], [
                'part_number'    => ($i + 1),
                'content_length' => $chunk['content_length'],
                'content_md5'    => base64_encode($chunk['md5'])
            ]);
        }

        $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);
        $responseData = $this->fromJson($response->getBody());

        $id = Arr::get($responseData, 'data.id', null);
        $uuid = Arr::get($responseData, 'data.latest_document_version.uuid', null);
        $multiparts = Arr::get($responseData, 'data.latest_document_version.multiparts', []);
        $uploadHeaders = Arr::get($responseData, 'data.latest_document_version.put_headers', []);
        $defaultHeaders = [];

        foreach ($uploadHeaders as $header) {
            $defaultHeaders[$header['name']] = $header['value'];
        }

        try {
            $this->uploadParts($initialChunks, $multiparts, $defaultHeaders);
            $this->cleanupMultiparts($initialChunks);
        } catch (Exception $e) {
            $this->cleanupMultiparts($initialChunks);
            $this->cleanupMultiparts($chunks);

            throw $e;
        }

        if (!empty($chunks)) {
            $extraChunkGroups = array_chunk($chunks, 50);
            $chunkNum = 51;

            $chunkParams = [
                'form_params' => [
                    'data' => [
                        'uuid'           => $uuid,
                        'fully_uploaded' => 'false'
                    ]
                ],
                'query'       => [
                    'fields' => $fields->__toString()
                ]
            ];

            // Iterate over the remaining chunks in groups of 50
            foreach ($extraChunkGroups as $chunkGroup) {
                $chunkParams['form_params']['data']['multiparts'] = [];

                // Add the details for each chunk
                foreach ($chunkGroup as $i => $chunk) {
                    array_push($chunkParams['form_params']['data']['multiparts'], [
                        'part_number'    => $chunkNum,
                        'content_length' => $chunk['content_length'],
                        'content_md5'    => base64_encode($chunk['md5'])
                    ]);
                }

                // Get the upload details for each chunk
                $chunkResponse = $this->client->put(sprintf('%s/%d.json', static::$basePath, $id), $chunkParams);
                $chunkData = $this->fromJson($chunkResponse->getBody());
                $chunkHeaders = Arr::get($chunkData, 'data.latest_document_version.put_headers', $uploadHeaders);
                $defaultChunkHeaders = [];

                foreach ($chunkHeaders as $chunkHeader) {
                    $defaultChunkHeaders[$chunkHeader['name']] = $chunkHeader['value'];
                }

                $chunkParts = Arr::get($chunkData, 'data.latest_document_version.multiparts', null);

                try {
                    $this->uploadParts($chunkGroup, $chunkParts, $defaultChunkHeaders);
                    $this->cleanupMultiparts($chunkGroup);
                } catch (Exception $e) {
                    $this->cleanupMultiparts($chunks);

                    throw $e;
                }
            }
        }

        $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id), [
            'form_params' => [
                'data' => [
                    'uuid'           => $uuid,
                    'fully_uploaded' => true
                ]
            ]
        ]);

        return $responseData;
    }

    /**
     * Splits a file into chunks of an arbitrary size.
     * Returns an array of the paths to each chunk file and their MD5 checksum.
     *
     * @param string $path
     * @param int    $chunkSize
     * @return array
     */
    protected function createFileChunks(string $path, $chunkSize = self::UPLOAD_CHUNK_SIZE): array
    {
        // Open the resource
        $file = fopen($path, 'r+');

        // Setup the initial offset and chunk starting number
        $offset = 0;
        $chunkNum = 1;
        $chunks = [];

        while ($chunkContent = stream_get_contents($file, $chunkSize, $offset)) {
            $chunkPath = sprintf('%s.%d', $path, $chunkNum);

            // Write the chunk
            file_put_contents($chunkPath, $chunkContent);

            // Add to the list of chunks with the MD5 checksum and final byte size
            array_push($chunks, [
                'part_number'    => $chunkNum,
                'path'           => $chunkPath,
                'content_length' => filesize($chunkPath),
                'md5'            => md5_file($chunkPath)
            ]);

            // Increment the offset and chunk number.
            $offset += $chunkSize;
            $chunkNum++;
        }

        fclose($file);

        return $chunks;
    }

    /**
     * Accepts an array of part definitions (path, content_length, md5),
     * an array of multipart definitions (part_number, put_url, put_headers),
     * and an optional array of default headers.
     *
     * This will upload the individual file parts in $localParts to the put_url provided in $remoteParts.
     *
     * @param array $localParts
     * @param array $remoteParts
     * @param array $defaultHeaders
     */
    protected function uploadParts(array $localParts, array $remoteParts, array $defaultHeaders = [])
    {
        // Merge the local chunk details with the remote details
        $parts = array_map(function ($localPart) use ($remoteParts) {
            $matchingRemote = [];

            foreach ($remoteParts as $remotePart) {
                if ((int)$remotePart['part_number'] === $localPart['part_number']) {
                    $matchingRemote = $remotePart;
                    break;
                }
            }

            return array_merge($localPart, $matchingRemote);
        }, $localParts);

        $uploadClient = new Client;

        foreach ($parts as $part) {
            $partParams = [
                'headers' => $defaultHeaders,
                'body'    => fopen($part['path'], 'r')
            ];

            foreach ($part['put_headers'] as $partHeader) {
                $partParams['headers'][$partHeader['name']] = $partHeader['value'];
            }

            $uploadClient->put($part['put_url'], $partParams);
        }
    }

    /**
     * Delete part files once they are uploaded
     *
     * @param array $parts
     */
    protected function cleanupMultiparts(array $parts)
    {
        foreach ($parts as $part) {
            if (file_exists($part['path'])) {
                unlink($part['path']);
            }
        }
    }

    /**
     * Create a document and upload the entire file directly.
     *
     * @param string          $path
     * @param string          $name
     * @param string|int|null $id
     * @param array           $formData
     * @return array
     */
    protected function uploadDirect(string $path, string $name, $id = null, array $formData = [])
    {
        // Request the upload URL in the response
        $fields = new ResourceFieldset();
        $fields->addField('id');
        $fields->addField('latest_document_version{uuid,put_url,put_headers}');

        $formParams = [
            'data' => [
                'name' => $name
            ]
        ];

        if (!empty($formData)) {
            $formParams = array_merge($formParams, $formData);
        }

        if (!empty($id)) {
            $formParams['data']['parent'] = [
                'id'   => $id,
                'type' => 'Document'
            ];
        }

        // Form data to request an upload URL
        $params = [
            'form_params' => $formParams,
            'query'       => [
                'fields' => $fields->__toString()
            ]
        ];

        $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);
        $responseData = $this->fromJson($response->getBody());

        $id = Arr::get($responseData, 'data.id', null);
        $uuid = Arr::get($responseData, 'data.latest_document_version.uuid', null);
        $uploadUrl = Arr::get($responseData, 'data.latest_document_version.put_url', null);
        $uploadHeaders = Arr::get($responseData, 'data.latest_document_version.put_headers', []);

        $uploadParams = [
            'headers' => [],
            'body'    => fopen($path, 'r')
        ];

        foreach ($uploadHeaders as $header) {
            $uploadParams['headers'][$header['name']] = $header['value'];
        }

        (new Client)->put($uploadUrl, $uploadParams);

        $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id), [
            'form_params' => [
                'data' => [
                    'uuid'           => $uuid,
                    'fully_uploaded' => 'true'
                ]
            ]
        ]);

        return $responseData;
    }

    /**
     * Method: GET
     * Path: /documents/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#show
     * @param       $id
     * @param array $params
     * @return array
     */
    public function show($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%s.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: POST
     * Path: /documents.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Document#create
     * @param array       $data
     * @param null|string $path
     * @param null        $fields
     * @return array
     * @throws Exception
     */
    public function create(array $data, string $path = null, $fields = null)
    {
        $fields = new ResourceFieldset($fields);
        $fields->addField('id');
        $fields->addField('latest_document_version{uuid,fully_uploaded}');

        // Determine if a valid path exists and if multipart or regular upload is to be used.
        if (!empty($path) && is_string($path)) {
            if (!file_exists($path)) {
                throw new Exception(sprintf('File "%s" does not exist.', $path));
            }

            // Make sure a name is set for the file
            if (array_key_exists('name', $data) && !empty($data['name'])) {
                $name = $data['name'];
            } else {
                $name = pathinfo($path, PATHINFO_BASENAME);
            }

            // Request the multipart field if we are using multipart
            // If using multipart, the first 50 parts can be sent to the API on the initial request.
            // Parts after the first 50 need to be sent in a later request.
            // if (filesize($path) > self::UPLOAD_MULTIPART_THRESHOLD) {
            //     $responseData = $this->uploadMultipart($path, $name, null, ['data' => $data]);
            // }
            $responseData = $this->uploadDirect($path, $name, null, ['data' => $data]);
        } else {
            $params = [
                'form_params' => [
                    'data' => array_merge($data, ['latest_document_version' => true])
                ],
                'query'       => [
                    'fields' => $fields->__toString()
                ]
            ];

            $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);
            $responseData = $this->fromJson($response->getBody());
        }

        return $this->show($responseData['data']['id'], ['fields' => $fields->__toString()]);
    }

}
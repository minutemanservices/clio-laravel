<?php

namespace MinuteMan\Clio\Resources\Documents;

use MinuteMan\Clio\Resources\Base;

/**
 * Class DocumentArchives
 *
 * @package MinuteMan\Clio\Resources\Documents
 */
class DocumentArchive extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'document_archives';

    /**
     * Method: GET
     * Path: /document_archives/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentArchive#show
     * @param       $id
     * @param array $params
     * @return mixed
     */
    public function find($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /document_archives/{id}/download.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentArchive#download
     * @param       $id
     * @param array $params
     * @return mixed
     */
    public function download($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%d/download.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: POST
     * Path: /document_archives.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentArchive#create
     * @param array             $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data, $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }

        $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);

        return $this->fromJson($response->getBody());
    }

}
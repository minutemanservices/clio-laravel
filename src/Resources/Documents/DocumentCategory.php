<?php

namespace MinuteMan\Clio\Resources\Documents;

use MinuteMan\Clio\Resources\Base;

/**
 * Class DocumentCategory
 *
 * @package MinuteMan\Clio\Resources\Documents
 */
class DocumentCategory extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'document_categories';

    /**
     * Method: GET
     * Path: /document_categories.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentCategory#index
     * @param array $params
     * @return array|bool
     */
    public function get(array $params = [])
    {
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /document_categories/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentCategory#show
     * @param       $id
     * @param array $params
     * @return mixed
     */
    public function find($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: DELETE
     * Path: /document_categories/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentCategory#destroy
     * @param $id
     * @return bool
     */
    public function destroy($id): bool
    {
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $response->getStatusCode() === 204;
    }

    /**
     * Method: PATCH
     * Path: /document_categories/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentCategory#update
     * @param                     $id
     * @param array               $data
     * @param null|string|array   $fields
     * @return mixed
     */
    public function update($id, array $data, $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }

        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id), $params);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: POST
     * Path: /document_categories.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentCategory#create
     * @param array             $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data, $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }

        $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);

        return $this->fromJson($response->getBody());
    }


}
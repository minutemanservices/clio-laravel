<?php

namespace MinuteMan\Clio\Resources\Documents;

use MinuteMan\Clio\Resources\Base;

/**
 * Class DocumentAutomations
 *
 * @package MinuteMan\Clio\Resources\Documents
 */
class DocumentAutomation extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'document_automations';

    /**
     * Method: GET
     * Path: /document_automations.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentAutomation#index
     * @param array $params
     * @return array|bool
     */
    public function get(array $params = [])
    {
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /document_automations/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentAutomation#show
     * @param       $id
     * @param array $params
     * @return mixed
     */
    public function find($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /document_automations/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentAutomation#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: POST
     * Path: /document_automations.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/DocumentAutomation#create
     * @param array             $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data, $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }

        $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);

        return $this->fromJson($response->getBody());
    }


}
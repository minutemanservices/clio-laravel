<?php

namespace MinuteMan\Clio\Resources\Documents;

use MinuteMan\Clio\ResourceFieldset;
use MinuteMan\Clio\Resources\Base;

/**
 * Class Folders
 *
 * @package MinuteMan\Clio\Resources\Documents
 */
class Folder extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'folders';

    /**
     * Method: GET
     * Path: /folders.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#index
     * @param array $params
     * @return array|bool
     */
    public function get(array $params = [])
    {
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /folders/list.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#list
     * @param array $params
     * @return mixed
     */
    public function list(array $params = [])
    {
        $response = $this->client->get(sprintf('%s/list.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /folders/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#show
     * @param       $id
     * @param array $params
     * @return mixed
     */
    public function find($id, array $params = [])
    {
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: GET
     * Path: /folders/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: DELETE
     * Path: /folders/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#destroy
     * @param $id
     * @return bool
     */
    public function destroy($id): bool
    {
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $response->getStatusCode() === 204;
    }

    /**
     * Method: PATCH
     * Path: /folders/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#update
     * @param                     $id
     * @param array               $data
     * @param null|string|array   $fields
     * @return mixed
     */
    public function update($id, array $data, $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        $fields = new ResourceFieldset($fields);

        // Add fields if provided
        if ($fields->hasFields()) {
            $params['query'] = [
                'fields' => $fields->__toString()
            ];
        }

        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id), $params);

        return $this->fromJson($response->getBody());
    }

    /**
     * Method: POST
     * Path: /folders.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Folder#create
     * @param array             $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data, $fields = null)
    {
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        $fields = new ResourceFieldset($fields);

        // Add fields if provided
        if ($fields->hasFields()) {
            $params['query'] = [
                'fields' => $fields->__toString()
            ];
        }

        $response = $this->client->post(sprintf('%s.json', static::$basePath), $params);

        return $this->fromJson($response->getBody());
    }

}
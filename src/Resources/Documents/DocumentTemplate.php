<?php

namespace MinuteMan\Clio\Resources\Documents;

use MinuteMan\Clio\Resources\Base;

/**
 * Class DocumentTemplates
 *
 * @package MinuteMan\Clio\Resources\Documents
 */
class DocumentTemplate extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'document_templates';

}
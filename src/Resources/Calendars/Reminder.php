<?php

namespace MinuteMan\Clio\Resources\Calendars;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Reminders
 *
 * @package MinuteMan\Clio\Resources\Calendars
 */
class Reminder extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'reminders';
        
    /**
     * Return the data for all Reminders
     * Method: GET
     * Path: /reminders.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Reminder#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Reminder
     * Method: POST
     * Path: /reminders.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Reminder#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Reminder
     * Method: GET
     * Path: /reminders/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Reminder#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Reminder
     * Method: PATCH
     * Path: /reminders/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Reminder#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Reminder
     * Method: DELETE
     * Path: /reminders/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Reminder#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
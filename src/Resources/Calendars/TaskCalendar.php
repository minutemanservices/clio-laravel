<?php

namespace MinuteMan\Clio\Resources\Calendars;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TaskCalendars
 *
 * @package MinuteMan\Clio\Resources\Calendars
 */
class TaskCalendar extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'task_calendars';
        
    /**
     * Method: GET
     * Path: /task_calendars/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CalendarVisibility#show
     * @param $id
     * @param array $params = []
     * @return mixed
     */
    public function show($id, array $params = [])
    {
        $response = $this->client->get(sprintf('/%s/%d.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Method: PATCH
     * Path: /task_calendars/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CalendarVisibility#update
     * @param $id
     * @param array $data = []
     * @param array $params = []
     * @return mixed
     */
    public function update($id, array $data = [], array $params = [])
    {
        $response = $this->client->patch(sprintf('/%s/%d.json', static::$basePath, $id), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Calendars;

use MinuteMan\Clio\Resources\Base;

/**
 * Class CalendarVisibility
 *
 * @package MinuteMan\Clio\Resources\Calendars
 */
class CalendarVisibility extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'task_calendars';
        
    /**
     * Return the data for all CalendarVisibilities
     * Method: GET
     * Path: /task_calendars.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CalendarVisibility#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single CalendarVisibility
     * Method: GET
     * Path: /task_calendars/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CalendarVisibility#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single CalendarVisibility
     * Method: PATCH
     * Path: /task_calendars/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CalendarVisibility#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
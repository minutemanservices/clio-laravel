<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BillableClients
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class BillableClient extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'billable_clients';
        
    /**
     * Return the data for all BillableClients
     * Method: GET
     * Path: /billable_clients.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BillableClient#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
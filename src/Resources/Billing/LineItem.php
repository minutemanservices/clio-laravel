<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class LineItems
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class LineItem extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'line_items';
        
    /**
     * Return the data for LineItems events (BETA)
     * Method: GET
     * Path: /line_items/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/LineItem#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all LineItems
     * Method: GET
     * Path: /line_items.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/LineItem#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single LineItem
     * Method: PATCH
     * Path: /line_items/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/LineItem#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single LineItem
     * Method: DELETE
     * Path: /line_items/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/LineItem#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
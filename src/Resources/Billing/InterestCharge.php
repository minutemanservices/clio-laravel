<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class InterestCharges
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class InterestCharge extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'interest_charges';
        
    /**
     * Return the data for all InterestCharges
     * Method: GET
     * Path: /interest_charges.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/InterestCharge#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single InterestCharge
     * Method: DELETE
     * Path: /interest_charges/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/InterestCharge#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Bills
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class Bill extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'bills';
        
    /**
     * Returns the pre-rendered html for the Bill
     * Method: GET
     * Path: /bills/{id}/preview.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Bill#preview
     * @param $id
     * @return mixed
     */
    public function preview($id)
    {        
        $response = $this->client->get(sprintf('%s/%d/preview.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for Bills events (BETA)
     * Method: GET
     * Path: /bills/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Bill#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Bills
     * Method: GET
     * Path: /bills.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Bill#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Bill
     * Method: GET
     * Path: /bills/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Bill#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Bill
     * Method: PATCH
     * Path: /bills/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Bill#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete or void a Bill
     * Method: DELETE
     * Path: /bills/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Bill#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
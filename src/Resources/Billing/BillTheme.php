<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BillThemes
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class BillTheme extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'bill_themes';
        
    /**
     * Return the data for all BillThemes
     * Method: GET
     * Path: /bill_themes.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BillTheme#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single BillTheme
     * Method: PATCH
     * Path: /bill_themes/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BillTheme#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
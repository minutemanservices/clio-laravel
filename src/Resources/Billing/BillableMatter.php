<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class BillableMatters
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class BillableMatter extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'billable_matters';
        
    /**
     * Returns the unique identifiers of all BillableMatter
     * Method: GET
     * Path: /billable_matters/ids.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BillableMatter#ids
     * @param array $params
     * @return mixed
     */
    public function ids(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/ids.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all BillableMatters
     * Method: GET
     * Path: /billable_matters.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/BillableMatter#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
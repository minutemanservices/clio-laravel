<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ClioPaymentsCards
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class ClioPaymentsCard extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'credit_cards';
        
    /**
     * Return the data for a single ClioPaymentsCard
     * Method: POST
     * Path: /credit_cards.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ClioPaymentsCard#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
    
}
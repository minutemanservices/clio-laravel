<?php

namespace MinuteMan\Clio\Resources\Billing;

use MinuteMan\Clio\Resources\Base;

/**
 * Class CreditCards
 *
 * @package MinuteMan\Clio\Resources\Billing
 */
class CreditCard extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'credit_cards';
        
    /**
     * Method: POST
     * Path: /credit_cards.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ClioPaymentsCard#create
     * @param array $data = []
     * @param array $params = []
     * @return mixed
     */
    public function create(array $data = [], array $params = [])
    {
        $response = $this->client->post(sprintf('/%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
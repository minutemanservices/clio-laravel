<?php

namespace MinuteMan\Clio\Resources\CustomFields;

use MinuteMan\Clio\Resources\Base;

/**
 * Class CustomFields
 *
 * @package MinuteMan\Clio\Resources\CustomFields
 */
class CustomField extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'custom_fields';
        
    /**
     * Return the data for CustomFields events (BETA)
     * Method: GET
     * Path: /custom_fields/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomField#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all CustomFields
     * Method: GET
     * Path: /custom_fields.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomField#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new CustomField
     * Method: POST
     * Path: /custom_fields.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomField#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single CustomField
     * Method: GET
     * Path: /custom_fields/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomField#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single CustomField
     * Method: PATCH
     * Path: /custom_fields/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomField#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single CustomField
     * Method: DELETE
     * Path: /custom_fields/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomField#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
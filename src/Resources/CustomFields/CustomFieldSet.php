<?php

namespace MinuteMan\Clio\Resources\CustomFields;

use MinuteMan\Clio\Resources\Base;

/**
 * Class CustomFieldSets
 *
 * @package MinuteMan\Clio\Resources\CustomFields
 */
class CustomFieldSet extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'custom_field_sets';
        
    /**
     * Return the data for CustomFieldSets events (BETA)
     * Method: GET
     * Path: /custom_field_sets/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomFieldSet#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all CustomFieldSets
     * Method: GET
     * Path: /custom_field_sets.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CustomFieldSet#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
    
}
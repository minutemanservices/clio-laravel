<?php

namespace MinuteMan\Clio\Resources\Contacts;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Contacts
 *
 * @package MinuteMan\Clio\Resources\Contacts
 */
class Contact extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'contacts';
        
    /**
     * Return the data for Contacts events (BETA)
     * Method: GET
     * Path: /contacts/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Contact#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Contacts
     * Method: GET
     * Path: /contacts.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Contact#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new Contact
     * Method: POST
     * Path: /contacts.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Contact#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Contact
     * Method: GET
     * Path: /contacts/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Contact#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single Contact
     * Method: PATCH
     * Path: /contacts/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Contact#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single Contact
     * Method: DELETE
     * Path: /contacts/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Contact#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
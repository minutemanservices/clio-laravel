<?php

namespace MinuteMan\Clio\Resources\Tasks;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TaskTemplateLists
 *
 * @package MinuteMan\Clio\Resources\Tasks
 */
class TaskTemplateList extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'task_template_lists';
        
    /**
     * Return the data for all TaskTemplateLists
     * Method: GET
     * Path: /task_template_lists.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplateList#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new TaskTemplateList
     * Method: POST
     * Path: /task_template_lists.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplateList#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single TaskTemplateList
     * Method: GET
     * Path: /task_template_lists/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplateList#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single TaskTemplateList
     * Method: PATCH
     * Path: /task_template_lists/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplateList#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single TaskTemplateList
     * Method: DELETE
     * Path: /task_template_lists/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplateList#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
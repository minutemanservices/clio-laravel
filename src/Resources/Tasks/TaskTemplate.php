<?php

namespace MinuteMan\Clio\Resources\Tasks;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TaskTemplates
 *
 * @package MinuteMan\Clio\Resources\Tasks
 */
class TaskTemplate extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'task_templates';
        
    /**
     * Return the data for all TaskTemplates
     * Method: GET
     * Path: /task_templates.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplate#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new TaskTemplate
     * Method: POST
     * Path: /task_templates.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplate#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single TaskTemplate
     * Method: GET
     * Path: /task_templates/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplate#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single TaskTemplate
     * Method: PATCH
     * Path: /task_templates/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplate#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single TaskTemplate
     * Method: DELETE
     * Path: /task_templates/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskTemplate#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
<?php

namespace MinuteMan\Clio\Resources\Tasks;

use MinuteMan\Clio\Resources\Base;

/**
 * Class TaskTypes
 *
 * @package MinuteMan\Clio\Resources\Tasks
 */
class TaskType extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'task_types';
        
    /**
     * Return the data for all TaskTypes
     * Method: GET
     * Path: /task_types.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskType#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new TaskType
     * Method: POST
     * Path: /task_types.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskType#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single TaskType
     * Method: GET
     * Path: /task_types/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskType#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single TaskType
     * Method: PATCH
     * Path: /task_types/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/TaskType#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
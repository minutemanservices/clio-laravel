<?php

namespace MinuteMan\Clio\Resources\Imports;

use MinuteMan\Clio\Resources\Base;

/**
 * Class ImportDetails
 *
 * @package MinuteMan\Clio\Resources\Imports
 */
class ImportDetail extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'import_details';
        
    /**
     * Preview first record of an ImportDetail
     * Method: GET
     * Path: /import_details/{id}/preview.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#preview
     * @param $id
     * @return mixed
     */
    public function preview($id)
    {        
        $response = $this->client->get(sprintf('import_details/%d/preview.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Start the ImportDetail
     * Method: PATCH
     * Path: /import_details/{id}/start.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#start
     * @param $id
     * @return mixed
     */
    public function start($id)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('import_details/%d/start.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Stop the ImportDetail
     * Method: PATCH
     * Path: /import_details/{id}/stop.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#stop
     * @param $id
     * @return mixed
     */
    public function stop($id)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('import_details/%d/stop.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Undo the ImportDetail
     * Method: PATCH
     * Path: /import_details/{id}/undo.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#undo
     * @param $id
     * @return mixed
     */
    public function undo($id)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('import_details/%d/undo.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Delete a single ImportDetail
     * Method: DELETE
     * Path: /import_details/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id): bool
    {        
        $response = $this->client->delete(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single ImportDetail
     * Method: GET
     * Path: /import_details/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Update a single ImportDetail
     * Method: PATCH
     * Path: /import_details/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#update
     * @param $id
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function update($id, array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->patch(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Download error file of an ImportDetail
     * Method: GET
     * Path: /import_details/{id}/download_errors.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#download_errors
     * @param $id
     * @return mixed
     */
    public function download_errors($id)
    {        
        $response = $this->client->get(sprintf('import_details/%d/download_errors.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all ImportDetails
     * Method: GET
     * Path: /import_details.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Create a new ImportDetail
     * Method: POST
     * Path: /import_details.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/ImportDetail#create
     * @param array $data
     * @param null|string|array $fields
     * @return mixed
     */
    public function create(array $data = [], array $fields = null)
    {            
        $params = [
            'form_params' => [
                'data' => $data
            ]
        ];

        // Add fields if provided
        if (!empty($fields)) {
            if (is_string($fields)) {
                $params['query'] = $fields;
            } else if (is_array($fields)) {
                $params['query'] = implode(',', $fields);
            }
        }
                
        $response = $this->client->post(sprintf('%s.json', static::$basePath));

        return $this->fromJson($response->getBody());
    }
    
}
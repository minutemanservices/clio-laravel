<?php

namespace MinuteMan\Clio\Resources\Payments;

use MinuteMan\Clio\Resources\Base;

/**
 * Class Allocations
 *
 * @package MinuteMan\Clio\Resources\Payments
 */
class Allocation extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'allocations';
        
    /**
     * Return the data for Allocations events (BETA)
     * Method: GET
     * Path: /allocations/events.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Allocation#events
     * @param array $params
     * @return mixed
     */
    public function events(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s/events.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for all Allocations
     * Method: GET
     * Path: /allocations.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Allocation#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single Allocation
     * Method: GET
     * Path: /allocations/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/Allocation#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}
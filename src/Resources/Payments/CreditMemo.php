<?php

namespace MinuteMan\Clio\Resources\Payments;

use MinuteMan\Clio\Resources\Base;

/**
 * Class CreditMemos
 *
 * @package MinuteMan\Clio\Resources\Payments
 */
class CreditMemo extends Base
{

    /**
     * @var string
     */
    public static $basePath = 'credit_memos';
        
    /**
     * Return the data for all CreditMemos
     * Method: GET
     * Path: /credit_memos.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CreditMemo#index
     * @param array $params
     * @return mixed
     */
    public function get(array $params = [])
    {        
        $response = $this->client->get(sprintf('%s.json', static::$basePath), ['query' => $params]);

        return $this->fromJson($response->getBody());
    }
            
    /**
     * Return the data for a single CreditMemo
     * Method: GET
     * Path: /credit_memos/{id}.json
     *
     * @link https://app.clio.com/api/v4/documentation?#operation/CreditMemo#show
     * @param $id
     * @return mixed
     */
    public function show($id)
    {        
        $response = $this->client->get(sprintf('%s/%d.json', static::$basePath, $id));

        return $this->fromJson($response->getBody());
    }
    
}